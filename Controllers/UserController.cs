﻿using BCrypt.Net;
using crud_test.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.Scripting;
using Newtonsoft.Json;

namespace crud_test.Controllers
{
    public class UserController : Controller
    {
        static List<UserViewModel> userViewModels = new List<UserViewModel>();
        private readonly ILogger<UserController> _logger;

        public UserController(ILogger<UserController> logger)
        {
            _logger = logger;
        }
        public IActionResult Create()
        {
            return View();
        }
        public IActionResult List()
        {
            string s = HttpContext.Session.GetString("myList");
            if (!string.IsNullOrEmpty(s)) {
                var model = JsonConvert.DeserializeObject<List<UserViewModel>>(s);
                return View(model);
            }
            return View();
        }
        public IActionResult Edit(Guid Id)
        {
            string s = HttpContext.Session.GetString("myList");
            var model = JsonConvert.DeserializeObject<List<UserViewModel>>(s);
            var data = model?.Where(model => model.Id == Id).FirstOrDefault();
            return View(data);
        }
        [HttpPost]
        public IActionResult Update([FromForm] UserViewModel user)
        {
            //var id = Request.Form["Id"];
            //var name = Request.Form["Name"];
            //var surname = Request.Form["Surname"];
            //var mobileNo = Request.Form["MobileNo"];

            //UserViewModel userViewModel = new UserViewModel
            //{
            //    Id = user.Id,
            //    Name = user.Name,
            //    Surname = user.Surname,
            //    MobileNo = user.MobileNo,
            //    Role = user.Role
            //};
            string s = HttpContext.Session.GetString("myList");
            var list = JsonConvert.DeserializeObject<List<UserViewModel>>(s);
            var data = list?.Where(model => model.Id == user.Id).FirstOrDefault();

            data.Id = user.Id;
            data.Name = user.Name;
            data.Surname = user.Surname;
            data.Role = user.Role;

            list.Add(data);
            list = list.Distinct().ToList();

            var jsonList = JsonConvert.SerializeObject(list);
            HttpContext.Session.Remove("myList");
            HttpContext.Session.SetString("myList", jsonList);
            return RedirectToAction("List");
        }
        public IActionResult Delete(Guid Id)
        {
            string s = HttpContext.Session.GetString("myList");
            var list = JsonConvert.DeserializeObject<List<UserViewModel>>(s);
            var data = list?.Where(model => model.Id == Id).FirstOrDefault();
            list.Remove(data);
            userViewModels = [];

            var jsonList = JsonConvert.SerializeObject(list);
            HttpContext.Session.Clear();
            HttpContext.Session.SetString("myList", jsonList);

            return RedirectToAction("List");
        }
        [HttpPost]
        public IActionResult ReisterUser([FromForm] UserViewModel user)
        {
            if (!this.ModelState.IsValid)
            {
                return View("Create",user);
            }
            //var name = Request.Form["Name"];
            //var surname = Request.Form["Surname"];
            //var mobileNo = Request.Form["MobileNo"];
            //var username = Request.Form["Username"];
            //var password = Request.Form["Password"];

            //UserViewModel userViewModel = new UserViewModel {
            //    Id = Guid.NewGuid(),
            //    Name = user.Name,
            //    Surname = surname,
            //    MobileNo = mobileNo,
            //    Username = username,
            //    Password = BCrypt.Net.BCrypt.HashPassword(password, SaltRevision.Revision2) 
            //};
            user.Id = Guid.NewGuid();
            user.Password = BCrypt.Net.BCrypt.HashPassword(user.Password, SaltRevision.Revision2);// var iscorrect = BCrypt.Net.BCrypt.Verify(model.Password, storedPassword)



            userViewModels.Add(user);
            var s = JsonConvert.SerializeObject(userViewModels);
            HttpContext.Session.SetString("myList", s);

            return RedirectToAction("Login");
        }
        public IActionResult Login()
        {
          
            return View();
        }
        [HttpPost]
        public IActionResult LoginUser([FromForm] LoginViewModel user)
        {
            if (!this.ModelState.IsValid)
            {
                return View(user);
            }
            string s = HttpContext.Session.GetString("myList");
            var list = JsonConvert.DeserializeObject<List<UserViewModel>>(s);
            var data = list?.Where(model => model.Username == user.Username).FirstOrDefault();

            var iscorrect = BCrypt.Net.BCrypt.Verify(user.Password, data.Password);
            if (iscorrect)
            {
                TempData["Userole"] = data.Role;
                return RedirectToAction("List");
            }

            return View();
        }
    }
}
