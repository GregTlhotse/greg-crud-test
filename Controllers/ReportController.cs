﻿using crud_test.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace crud_test.Controllers
{
    
    [ApiController]
    
    public class ReportController  : ControllerBase
    {
        private IConfiguration _config;
        public ReportController(IConfiguration config)
        {
            _config = config;
        }
        
        [HttpPost("login")]
        
        public IActionResult Login([FromBody] LoginViewModel user)
        {
            string s = HttpContext.Session.GetString("myList");
            var list = JsonConvert.DeserializeObject<List<UserViewModel>>(s);
            var data = list?.Where(model => model.Username == user.Username).FirstOrDefault();

            var iscorrect = BCrypt.Net.BCrypt.Verify(user.Password, data.Password);
            if (iscorrect)
            {
                var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
                var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

                var Sectoken = new JwtSecurityToken(_config["Jwt:Issuer"],
                  _config["Jwt:Issuer"],
                  null,
                  expires: DateTime.Now.AddMinutes(120),
                  signingCredentials: credentials);

                var token = new JwtSecurityTokenHandler().WriteToken(Sectoken);

                return Ok(token);
            }
            return Ok("Failed to login!");
        }
        [Authorize]
        [HttpGet("list-user-with-role")]
        public IActionResult Post(string role)
        {
            string s = HttpContext.Session.GetString("myList");
            if (!string.IsNullOrEmpty(s))
            {
                var list = JsonConvert.DeserializeObject<List<UserViewModel>>(s);
                list = list.Where(list => list.Role == role).ToList(); 
                return Ok(list);
            }
            return Ok("Not found!");
        }
    }
}
